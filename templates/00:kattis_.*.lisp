;;;; open.kattis.com problems
;;;; by Michael Reis
;;;; michael@mtreis86.com

#|

|#

(defun kattis ()
  (let* ((source *standard-input*)
         (input (read source nil)))
    (format t "~A" input)))
