;;; General settings

(setq-default fill-column 80
              user-full-name "Michael Reis"
              user-mail-address "michael@mtreis86.com")

(setq confirm-kill-emacs nil
      create-lockfiles nil
      default-input-method "TeX"
      mouse-wheel-scroll-amount '(3)
      mouse-wheel-progressive-speed nil
      mouse-wheel-follow-mouse t
      scroll-step 1
      vc-follow-symlinks t)

;;; Packages

(def-package! all-the-icons-ivy
  :config
  (setq all-the-icons-ivy-file-commands
        '(counsel-find-file counsel-file-jump
                            counsel-recentf
                            counsel-projectile-find-file
                            counsel-projectile-find-dir))
  (all-the-icons-ivy-setup))

(def-package! evil-cleverparens
  :init
  (setq evil-move-beyond-eol t
        evil-cleverparens-swap-move-by-word-and-symbol t
        evil-cleverparens-use-regular-insert t)
  (add-hook! (lisp-mode emacs-lisp-mode clojure-mode)
    (evil-cleverparens-mode t)))

(after! org
  :config
  (setq org-directory (expand-file-name "~/Projects/Org")
        +org-dir org-directory
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-refile-targets '((nil :maxlevel . 5)
                             (org-agenda-files :maxlevel . 5))
        org-refile-use-outline-path 'file
        org-outline-path-complete-in-steps nil
        org-capture-templates
        '(("c" "Code Task" entry (file+headline org-default-notes-file
                                                "Coding Tasks")
           "* TODO %?\n  Entered on: %U - %a\n")
          ("t" "Task" entry (file+headline org-default-notes-file "Tasks")
           "* TODO %?\n  Entered on: %U")
          ("n" "Note" entry (file+olp+datetree org-default-notes-file)
           "* %?\n\n"))))

(after! imenu-anywhere
  (setq imenu-anywhere-buffer-filter-functions
        '(imenu-anywhere-same-project-p)))

(after! ivy
  (setq ivy-re-builders-alist '((t . ivy--regex-plus))))

(after! which-key
  (setq which-key-idle-delay 0.25
        which-key-idle-secondary-delay 0.25
        which-key-sort-order 'which-key-key-order-alpha))

(after! sly
  (setq sly-lisp-implementations
        '((sbcl ("sbcl")))
        sly-lisp-default 'sbcl
        common-lisp-hyperspec-root (expand-file-name
                                    "~/Builds/hyperspec/")
        sly-complete-symbol-function 'sly-flex-completions
        browse-url-browser-function 'eww-browse-url)
  (set-popup-rules!
    '(("^\\*sly-mrepl"       :side right :size 80 :slot 2 :quit nil :ttl nil)
      ("^\\*sly-compilation" :side right :size 80 :slot 1 :ttl nil))))

(after! smartparens
  (setq sp-show-pair-from-inside t
        sp-cancel-autoskip-on-backward-movement nil
        sp-highlight-pair-overlay nil
        sp-highlight-wrap-overlay nil
        sp-highlight-wrap-tag-overlay nil)
  (sp-pair "'" nil :actions :rem)
  (sp-pair "`" nil :actions :rem)
  (smartparens-global-strict-mode 1))

(add-hook! smartparens-enabled
  (hungry-delete-mode 1))

(def-package! hungry-delete
  :config
  (global-hungry-delete-mode))

(add-hook! prog-mode
  (aggressive-indent-mode 1))

(def-package! evil-lisp-state
  :init (setq evil-lisp-state-global t)
  :config (evil-lisp-state-leader "SPC l"))

(after! cider
  (setq cider-clojure-cli-global-options "-A:dev:bench:trace"))

(after! magit
  (magit-wip-mode)
  (setq magit-delete-by-moving-to-trash nil))

(after! elfeed
  (defalias 'elfeed-toggle-star
    (elfeed-expose #'elfeed-search-toggle-all 'starred))
  (setq-default elfeed-search-filter "@1-month-ago +unread")
  (setq elfeed-search-title-max-width 150
        elfeed-search-date-format '("%Y-%m-%d %I:%M%P" 20 :left)))

;;; Fixes

;; I dislike Emacs asking me if I'm sure I want to quit with sub-processes
;; running.
(defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
  (cl-letf (((symbol-function #'process-list) (lambda ())))
    ad-do-it))

;; I don't want comment characters to be automatically included with a new line
;; from a commented line, only when I type them explicitly.
(advice-remove 'newline-and-indent #'doom*newline-and-indent)
(advice-remove 'evil-insert-newline-below
               #'+evil*insert-newline-below-and-respect-comments)
(advice-remove 'evil-open-below
               #'+evil*insert-newline-below-and-respect-comments)

;; Create a new workspace when switching projects.
(setq +workspaces-on-switch-project-behavior t)

;; Turn autosave back on
(setq auto-save-visited-mode t)

;;; Included files

(load! "+bindings")
