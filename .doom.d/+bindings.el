;;; General

(define-key minibuffer-inactive-mode-map [mouse-1] nil)

(global-unset-key [insert])
(global-unset-key [insertchar])
(global-unset-key [C-down-mouse-1])
(global-unset-key [C-down-mouse-2])
(global-unset-key [C-down-mouse-3])

;; Swap parans and brackets
(keyboard-translate ?\( ?\[)
(keyboard-translate ?\) ?\])
(keyboard-translate ?\[ ?\()
(keyboard-translate ?\] ?\))

(map!
 [remap evil-jump-to-tag] #'projectile-find-tag
 [remap find-tag] #'projectile-find-tag
 [remap move-beginning-of-line] #'mfiano/smarter-move-beginning-of-line
 :i [remap newline] #'newline-and-indent
 :gnvime "M-x" #'execute-extended-command
 :gnvime "A-x" #'execute-extended-command
 :gnvime "M-;" #'eval-expression
 :nv "." #'evil-repeat
 :n "M-a" #'mark-whole-buffer
 :i [tab] #'+company/complete
 :n "M-=" (λ! (text-scale-set 0))
 :n "M-t" #'+workspace/new
 :n "M-1" (λ! (+workspace/switch-to 0))
 :n "M-2" (λ! (+workspace/switch-to 1))
 :n "M-3" (λ! (+workspace/switch-to 2))
 :n "M-4" (λ! (+workspace/switch-to 3))
 :n "M-5" (λ! (+workspace/switch-to 4))
 :n "M-6" (λ! (+workspace/switch-to 5))
 :n "M-7" (λ! (+workspace/switch-to 6))
 :n "M-8" (λ! (+workspace/switch-to 7))
 :n "M-9" (λ! (+workspace/switch-to 8))
 :v "@" #'+evil:apply-macro
 :v "<" #'+evil/visual-dedent
 :v ">" #'+evil/visual-indent
 :nv "g-" #'+evil:narrow-buffer
 :n "g=" #'widen
 :n "gb" #'evil-jump-backward
 :n "gd" #'+lookup/definition
 :n "gD" #'+lookup/references
 :n "gf" #'+lookup/file
 :n "gh" #'+lookup/documentation
 :n "gp" #'+popup/other
 [f8] #'comment-region
 [S-f8] #'uncomment-region)

;;; Packages

(map!
 ;; comint
 (:when (featurep! :completion company)
   (:after comint
     :map comint-mode-map [tab] #'company-complete))
 ;; company
 (:after company
   (:map company-active-map
     "C-w" nil
     "C-h" #'company-show-doc-buffer
     [tab] #'company-indent-or-complete-common
     [backtab] #'company-select-previous
     (:when (featurep! :completion ivy)
       "C-SPC" #'counsel-company)))
 ;; evil
 (:after evil
   :textobj "x" #'evil-inner-xml-attr               #'evil-outer-xml-attr
   :textobj "a" #'evil-inner-arg                    #'evil-outer-arg
   :textobj "B" #'evil-textobj-anyblock-inner-block #'evil-textobj-anyblock-a-block
   :textobj "i" #'evil-indent-plus-i-indent         #'evil-indent-plus-a-indent
   :textobj "k" #'evil-indent-plus-i-indent-up      #'evil-indent-plus-a-indent-up
   :textobj "j" #'evil-indent-plus-i-indent-up-down #'evil-indent-plus-a-indent-up-down
   :n "gc" #'evil-commentary
   :nv [tab] #'+evil/matchit-or-toggle-fold)
 ;; evil-magit
 (:after evil-magit
   :map (magit-status-mode-map magit-revision-mode-map)
   :n "C-j" nil
   :n "C-k" nil)
 ;; evil-multiedit
 :nv "gm" #'evil-multiedit-match-all
 ;; expand-region
 :v "v" #'er/expand-region
 :v "V" #'er/contract-region
 ;; git-timemachine
 (:after git-timemachine
   (:map git-timemachine-mode-map
     :n "[" #'git-timemachine-show-previous-revision
     :n "]" #'git-timemachine-show-next-revision
     :n "gb" #'git-timemachine-blame
     :n "q" #'git-timemachine-quit))
 ;; help
 (:map* (help-mode-map helpful-mode-map)
        :n "[[" #'backward-button
        :n "]]" #'forward-button
        :n "o" #'ace-link-help
        :n "q" #'quit-window)
 ;; hl-todo
 :m "]t" #'hl-todo-next
 :m "[t" #'gl-todo-previous
 ;; markdown-mode
 (:after markdown-mode
   (:map markdown-mode-map
     "<backspace>" nil
     "<M-left>" nil
     "<M-right>" nil))
 ;; smartparens
 (:after smartparens
   :i "<M-backspace>" #'sp-backward-delete-word
   :localleader
   :map lisp-mode-map
   (:prefix ("x" . "Text")
     :desc "Slurp forwards" "f" #'sp-forward-slurp-sexp
     :desc "Slurp backwards" "d" #'sp-backward-slurp-sexp
     :desc "Barf forwards" "v" #'sp-forward-barf-sexp
     :desc "Barf backwards" "c" #'sp-backward-barf-sexp))
 ;; vc-annotate
 (:after vc-annotate
   :map vc-annotate-mode-map
   [remap quit-window] #'kill-this-buffer)
 ;; elfeed
 (:after elfeed
   :map elfeed-search-mode-map
   :desc "Show all" :n "g/a" (λ! () (elfeed-search-set-filter ""))
   :desc "Show unread" :n "g/u" (λ! () (elfeed-search-set-filter "+unread"))
   :desc "Past year" :n "g/y" (λ! () (elfeed-search-set-filter "@11-year-ago"))
   :desc "Past month" :n "g/m" (λ! () (elfeed-search-set-filter "@1-month-ago"))
   :desc "Past week" :n "g/w" (λ! () (elfeed-search-set-filter "@1-week-ago"))
   :desc "Past day" :n "g/d" (λ! () (elfeed-search-set-filter "@1-day-ago"))
   :desc "Show starred" :n "g/s" (λ! () (elfeed-search-set-filter "starred"))
   :desc "Toggle starred" :n "gts" (λ! () (elfeed-toggle-star))))



;; leader
(map!
 :leader
 :desc "M-x" "SPC" #'execute-extended-command
 :desc "Ex command" ";" #'evil-ex
 :desc "Universal argument" "u" #'universal-argument
 (:when (featurep! :completion ivy)
   :desc "Resume last search" "'" #'ivy-resume
   :desc "Bookmark set/jump" "RET" #'counsel-bookmark)
 (:prefix ("[" . "Previous")
   :desc "Argument" "a" #'evil-backward-arg
   :desc "Buffer" "b" #'previous-buffer
   :desc "Hunk" "d" #'git-gutter:previous-hunk
   :desc "Error" "e" #'previous-error
   :desc "Text size" "f" #'text-scale-decrease
   :desc "Spelling error" "s" #'evil-prev-flyspell-error
   :desc "Spelling correction" "S" #'flyspell-correct-previous-word-generic
   :desc "To do" "t" #'hl-todo-previous
   :desc "Workspace" "w" #'+workspace/switch-left)
 (:prefix ("]" . "Next")
   :desc "Argument" "a" #'evil-forward-arg
   :desc "Buffer" "b" #'next-buffer
   :desc "Hunk" "d" #'git-gutter:next-hunk
   :desc "Error" "e" #'next-error
   :desc "Text size" "f" #'text-scale-increase
   :desc "Spelling error" "s" #'evil-next-flyspell-error
   :desc "Spelling correction" "S" #'flyspell-correct-word-generic
   :desc "To do" "t" #'hl-todo-next
   :desc "Workspace" "w" #'+workspace/switch-right)
 (:prefix ([tab] . "workspace")
   :desc "Delete" "d" #'+workspace/delete
   :desc "Load from file" "l" #'+workspace/load
   :desc "Load session" "L" #'doom/load-session
   :desc "New" "n" #'+workspace/new
   :desc "Rename" "r" #'+workspace/rename
   :desc "Restore session" "R" #'doom/quickload-session
   :desc "Save to file" "s" #'+workspace/save
   :desc "Save session" "S" #'doom/save-session
   :desc "Delete buffers" "x" #'doom/kill-all-buffers
   :desc "Delete session" "X" #'+workspace/kill-session
   :desc "Switch to workspace 1" "1" (λ! (+workspace/switch-to 0))
   :desc "Switch to workspace 2" "2" (λ! (+workspace/switch-to 1))
   :desc "Switch to workspace 3" "3" (λ! (+workspace/switch-to 2))
   :desc "Switch to workspace 4" "4" (λ! (+workspace/switch-to 3))
   :desc "Switch to workspace 5" "5" (λ! (+workspace/switch-to 4))
   :desc "Switch to workspace 6" "6" (λ! (+workspace/switch-to 5))
   :desc "Switch to workspace 7" "7" (λ! (+workspace/switch-to 6))
   :desc "Switch to workspace 8" "8" (λ! (+workspace/switch-to 7))
   :desc "Switch to workspace 9" "9" (λ! (+workspace/switch-to 8))
   :desc "Switch workspace" [tab] #'+workspace/switch-to)
 (:prefix ("a" . "apps")
   :desc "Feed reader" "f" #'elfeed
   :desc "File manager" "F" #'dired-jump
   :desc "REPL" :n "r" #'+eval/open-repl-other-window :v "r" #'+eval:repl
   :desc "Terminal" "t" #'+term/open-popup)
 (:prefix ("b" . "buffer")
   (:when (featurep! :feature workspaces)
     :desc "Switch workspace buffer" "b" #'persp-switch-to-buffer)
   :desc "Switch" "B" #'switch-to-buffer
   :desc "Delete" "d" #'kill-this-buffer
   :desc "Delete other" "D" #'doom/kill-other-buffers
   :desc "New" "n" #'evil-buffer-new
   :desc "Scratch" "s" #'doom/open-scratch-buffer
   :desc "Sudo edit" "S" #'doom/sudo-this-file)
 (:prefix ("f" . "file")
   :desc "Find file in config" "c" (λ! (doom-project-find-file doom-private-dir))
   :desc "Browse config" "C" (λ! (doom-project-browse doom-private-dir))
   :desc "Find directory" "d" #'dired
   :desc "Delete" "D" #'doom/delete-this-file
   :desc "Find file in emacs.d" "e" (λ! (doom-project-find-file doom-emacs-dir))
   :desc "Browse emacs.d" "E" (λ! (doom-project-browse doom-emacs-dir))
   :desc "Find file" "f" #'find-file
   :desc "Sudo find file" "F" #'doom/sudo-find-file
   :desc "Recent" "r" #'recentf-open-files
   :desc "Rename file" "R" #'rename-file
   :desc "Save" "s" #'save-buffer)
 (:prefix ("F" . "frame")
   :desc "New" "n" #'make-frame
   :desc "Delete" "d" #'delete-frame)
 (:prefix ("g" . "git")
   :desc "Blame" "b" #'magit-blame-addition
   :desc "Clone" "c" #'+magit/clone
   :desc "Gist" "g" #'gist-region-or-buffer
   :desc "Gist (private)" "G" #'gist-region-or-buffer-private
   :desc "Browse issues tracker" "i" #'forge-browse-issues
   :desc "Magit buffer log" "l" #'magit-log-buffer-file
   :desc "Initialize new repo" "n" #'magit-init
   :desc "Browse remote" "o" #'+vc:git-browse
   :desc "Post comment" "p" #'forge-create-post
   :desc "Edit comment" "P" #'forge-edit-post
   :desc "Git revert hunk" "r" #'git-gutter:revert-hunk
   :desc "Git revert file" "R" #'vc-revert
   :desc "Magit status" "s" #'magit-status
   :desc "Git time machine" "t" #'git-timemachine-toggle)
 (:prefix ("h" . "help")
   :desc "Apropos" "a" #'apropos
   :desc "Describe char" "c" #'describe-char
   :desc "Describe function" "f" #'describe-function
   :desc "Describe face" "F" #'describe-face
   :desc "Info" "i" #'info-lookup-symbol
   :desc "Describe key" "k" #'describe-key
   :desc "Find library" "l" #'find-library
   :desc "Command log" "L" #'clm/toggle-command-log-buffer
   :desc "Describe minor modes" "m" #'doom/describe-active-minor-mode
   :desc "Describe major mode" "M" #'describe-mode
   :desc "Toggle profiler" "p" #'doom/toggle-profiler
   :desc "Reload theme" "r" #'doom/reload-theme
   :desc "Reload private config" "R" #'doom/reload
   :desc "Describe variable" "v" #'describe-variable
   :desc "Describe at point" "." #'helpful-at-point)
 (:prefix ("i" . "insert")
   (:when (featurep! :completion ivy)
     :desc "From evil registers" "r" #'counsel-evil-registers
     :desc "From kill-ring" "y" #'counsel-yank-pop))
 (:prefix ("o" . "org")
   :desc "Agenda" "a" #'org-agenda
   :desc "Add file to agenda" "A" #'org-agenda-file-to-front
   :desc "Capture" "c" #'org-capture
   :desc "Copy" "C" #'org-copy
   :desc "Find file" "f" (λ! (doom-project-find-file org-directory))
   :desc "Browse directory" "d" (λ! (doom-project-browse org-directory))
   :desc "Refile" "r" #'org-refile)
 (:prefix ("p" . "project")
   :desc "Find file" "f" (λ! (doom-project-browse (doom-project-root)))
   :desc "Kill buffers" "k" #'projectile-kill-buffers
   :desc "Switch project" "p" #'projectile-switch-project
   :desc "Recent project files" "r" #'projectile-recentf
   :desc "Replace text" "R" #'projectile-replace
   :desc "Save buffers" "s" #'projectile-save-project-buffers
   :desc "Toggle sidebar" "S" #'+treemacs/toggle
   :desc "List project tasks" "t" #'+ivy/tasks
   :desc "Open terminal" "T" #'+term/open-popup-in-project
   :desc "Invalidate cache" "x" #'projectile-invalidate-cache)
 (:prefix ("q" . "quit")
   :desc "Quit Emacs" "q" #'evil-quit-all
   :desc "Save and quit" "Q" #'evil-save-and-quit
   :desc "Restart" "r" #'restart-emacs
   :desc "Restart and restore workspaces" "R" #'doom/restart-and-restore
   :desc "Quit (forget session)" "X" #'+workspace/kill-session-and-quit)
 (:prefix ("s" . "search")
   (:when (featurep! :completion ivy)
     :desc "Buffer" "b" #'swiper
     :desc "Directory" "d" #'+ivy/project-search-from-cwd
     :desc "Project" "p" #'+ivy/project-search)
   :desc "Multi-edit" "m" #'evil-multiedit-match-all
   :desc "Online providers" "o" #'+lookup/online-select
   :desc "Tags" "t" #'imenu
   :desc "Tags across buffers" "T" #'imenu-anywhere)
 (:prefix ("t" . "toggle")
   :desc "Big font mode" "b" #'doom-big-font-mode
   :desc "Flycheck" "f" #'flycheck-mode
   :desc "Frame fullscreen" "F" #'toggle-frame-fullscreen
   :desc "Line numbers" "l" #'doom/toggle-line-numbers
   :desc "Popup window" "p" #'+popup/toggle
   :desc "Flyspell" "s" #'flyspell-mode)
 (:prefix ("w" . "window")
   :desc "Delete" "d" #'evil-window-delete
   :desc "Grow" "g" #'doom/window-enlargen
   :desc "Redo" "r" #'winner-redo
   :desc "Swap" "s" #'ace-swap-window
   :desc "Undo" "u" #'winner-undo
   :desc "Balance" "=" #'balance-windows
   :desc "Split horizontal" "-" #'evil-window-split
   :desc "Split vertical" "|" #'evil-window-vsplit
   :desc "Switch" "w" #'ace-window))
