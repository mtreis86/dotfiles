(setq-default doom-leader-key "SPC"
              doom-localleader-key ","
              doom-font (font-spec :family "source code pro" :size 14)
              doom-big-font (font-spec :family "source code pro" :size 28))

(doom! :feature

       :config
       (default +smartparans)

       :completion
       (company +tng)
       (ivy +fuzzy)

       :ui
       doom
       fill-column
       hl-todo
       modeline
       nav-flash
       ophints
       treemacs
       (popup +defaults)
       vc-gutter
       window-select
       workspaces

       :editor
       (evil +everywhere)
       fold
       multiple-cursors
       rotate-text

       :emacs
       (dired +icons)
       electric
       imenu
       term
       vc

       :tools
       eval
       (flycheck +childframe)
       flyspell
       (gist +view)
       (lookup +docsets)
       magit
       pdf
       rgb
       upload

       :lang
       common-lisp
       data
       emacs-lisp
       latex
       javascript
       (org +attach +babel +capture +export +present)
       python
       sh

       :app
       irc
       (rss +org)
       write)
